package br.com.motta.douglas.apigithubjava.activity;

import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import br.com.motta.douglas.apigithubjava.R;
import br.com.motta.douglas.apigithubjava.adapter.RepositoryAdapter;
import br.com.motta.douglas.apigithubjava.entity.Repository;
import br.com.motta.douglas.apigithubjava.entity.RepositoryCatalog;
import br.com.motta.douglas.apigithubjava.entity.VolleySingleton;
import br.com.motta.douglas.apigithubjava.interfaces.Transaction;
import br.com.motta.douglas.apigithubjava.ws.GsonRequest;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements Transaction {
    private static final String title = "Github Api Java";

    protected ArrayList<Repository> mListRepositories;
    protected RepositoryAdapter repositoryAdapter;
    protected ActionBarDrawerToggle mActionBarDrawerToggle;

    @ViewById
    protected RecyclerView rvGithub;

    @ViewById
    protected SwipeRefreshLayout srlGithub;

    @ViewById
    protected DrawerLayout drawerLayout;

    private int page = 1;

    @AfterViews
    void init() {
        setUpToolbar(title);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);

        initDrawerLayout();
        initAdapter();
        setUpRecyclerView();
        doRequestRepositories();
        registerListeners();
    }

    private void initDrawerLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mainToolbar, R.string.drawer_open, R.string.drawer_close);

        drawerLayout.addDrawerListener(mActionBarDrawerToggle);
    }

    private void initAdapter() {
        mListRepositories = new ArrayList<>();
        repositoryAdapter = new RepositoryAdapter(MainActivity.this, mListRepositories);
    }

    private void setUpRecyclerView() {
        rvGithub.setAdapter(repositoryAdapter);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rvGithub.setLayoutManager(layout);
    }

    private void doRequestRepositories() {
        if (mListRepositories.size() == 0) {
            srlGithub.setRefreshing(true);
            VolleySingleton.execute(this);
        }
    }

    private void registerListeners() {
        repositoryAdapter.setOnItemClickListener(new RepositoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Intent intent = new Intent(MainActivity.this, PullRequestActivity_.class)
                        .putExtra(PullRequestActivity.EXTRA_REPOSITORY, mListRepositories.get(position));

                startActivity(intent);
            }
        });

        rvGithub.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager llm = (LinearLayoutManager) rvGithub.getLayoutManager();

                if (mListRepositories != null && mListRepositories.size() == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    srlGithub.setRefreshing(true);
                    VolleySingleton.execute(MainActivity.this);
                }
            }
        });

        srlGithub.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlGithub.setRefreshing(false);
            }
        });
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public void doAfter() {
        GsonRequest gsonObjectRequest = new GsonRequest(Repository.getUrl(page++), RepositoryCatalog.class, null, new Response.Listener<RepositoryCatalog>() {
            @Override
            public void onResponse(RepositoryCatalog response) {
                repositoryAdapter.addList(response.items);
                srlGithub.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(gsonObjectRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
}
