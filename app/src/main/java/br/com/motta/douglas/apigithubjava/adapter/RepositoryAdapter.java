package br.com.motta.douglas.apigithubjava.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import br.com.motta.douglas.apigithubjava.R;
import br.com.motta.douglas.apigithubjava.entity.Repository;
import br.com.motta.douglas.apigithubjava.entity.VolleySingleton;

/**
 * Created by douglas on 21/03/17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter {
    private List<Repository> mRepositories;
    private Context mContext;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public RepositoryAdapter(Context context, List<Repository> repositories) {
        this.mRepositories = repositories;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.mContext).inflate(R.layout.repository_item, parent, false);

        return new RepositoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RepositoryViewHolder repositoryHolder = (RepositoryViewHolder) holder;

        Repository repository = mRepositories.get(position);

        if (repository != null) {
            repositoryHolder.tvName.setText(repository.getName());
            repositoryHolder.tvDescription.setText(repository.getDescription() != null && repository.getDescription().length() > 60 ? repository.getDescription().substring(0, 60) + "..." : repository.getDescription());
            repositoryHolder.tvForksCount.setText(String.valueOf(repository.getForksCount()));
            repositoryHolder.tvStargazersCount.setText(String.valueOf(repository.getStargazersCount()));
            repositoryHolder.tvLogin.setText(repository.getOwner().getLogin());
            repositoryHolder.ivAvatar.setImageUrl(repository.getOwner().getAvatarUrl(), VolleySingleton.getInstance(mContext).getImageLoader());
        }
    }

    @Override
    public int getItemCount() {
        if (this.mRepositories != null) {
            return this.mRepositories.size();
        }

        return 0;
    }


    public void addList(List<Repository> listRepository) {
        mRepositories.addAll(listRepository);
        notifyDataSetChanged();
    }

    class RepositoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvDescription;
        TextView tvForksCount;
        TextView tvStargazersCount;
        TextView tvLogin;
        NetworkImageView ivAvatar;


        public RepositoryViewHolder(final View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvForksCount = (TextView) itemView.findViewById(R.id.tvForksCount);
            tvStargazersCount = (TextView) itemView.findViewById(R.id.tvStargazersCount);
            tvLogin = (TextView) itemView.findViewById(R.id.tvLogin);
            ivAvatar = (NetworkImageView) itemView.findViewById(R.id.ivAvatar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
