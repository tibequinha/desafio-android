package br.com.motta.douglas.apigithubjava.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by douglas on 21/03/17.
 */

public class PullRequest implements Parcelable {
    public static final String URL_PULL_REQUESTS = "https://api.github.com/repos/owner/repository/pulls";
    public static final String OPENED = "open";
    public static final String CLOSED = "close";

    private String title;
    private Date createdAt;
    private String body;
    private User user;
    private String state;
    private String htmlUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }



    public static String getUrl(String owner, String repository) {
        return URL_PULL_REQUESTS.replace("owner", owner).replace("repository", repository);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeString(this.body);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.state);
        dest.writeString(this.htmlUrl);
    }

    protected PullRequest(Parcel in) {
        this.title = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        this.body = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.state = in.readString();
        this.htmlUrl = in.readString();
    }

    public static final Creator<PullRequest> CREATOR = new Creator<PullRequest>() {
        @Override
        public PullRequest createFromParcel(Parcel source) {
            return new PullRequest(source);
        }

        @Override
        public PullRequest[] newArray(int size) {
            return new PullRequest[size];
        }
    };
}
