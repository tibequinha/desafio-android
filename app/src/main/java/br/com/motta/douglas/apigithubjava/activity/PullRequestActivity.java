package br.com.motta.douglas.apigithubjava.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import br.com.motta.douglas.apigithubjava.R;
import br.com.motta.douglas.apigithubjava.adapter.PullRequestAdapter;
import br.com.motta.douglas.apigithubjava.entity.PullRequest;
import br.com.motta.douglas.apigithubjava.entity.Repository;
import br.com.motta.douglas.apigithubjava.entity.VolleySingleton;
import br.com.motta.douglas.apigithubjava.interfaces.Transaction;
import br.com.motta.douglas.apigithubjava.ws.GsonRequest;

@EActivity(R.layout.activity_pull_request)
public class PullRequestActivity extends BaseActivity implements Transaction {
    public static final String EXTRA_REPOSITORY = "extra_repository";

    protected Repository mRepository;
    protected PullRequestAdapter pullRequestAdapter;

    @InstanceState
    protected ArrayList<PullRequest> mPullRequests;

    @ViewById
    protected RecyclerView rvGithub;

    @ViewById
    protected SwipeRefreshLayout srlGithub;

    @ViewById
    protected LinearLayout llCountPullRequests;

    @ViewById
    protected TextView tvCountPullRequests;

    @AfterViews
    void init() {
        mRepository = getIntent().getParcelableExtra(EXTRA_REPOSITORY);

        setUpToolbar(mRepository.getName());
        initAdapter();
        setUpRecyclerView();
        doRequestPullRequests();
        registerListeners();
    }

    private void initAdapter() {
        mPullRequests = new ArrayList<>();
        pullRequestAdapter = new PullRequestAdapter(this, mPullRequests);
    }

    private void setUpRecyclerView() {
        rvGithub.setAdapter(pullRequestAdapter);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvGithub.setLayoutManager(layout);
    }

    private void doRequestPullRequests() {
        srlGithub.setRefreshing(true);
        VolleySingleton.execute(this);
    }

    private void registerListeners() {
        pullRequestAdapter.setOnItemClickListener(new PullRequestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(mPullRequests.get(position).getHtmlUrl()));
                startActivity(i);
            }
        });

        srlGithub.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlGithub.setRefreshing(true);
                VolleySingleton.execute(PullRequestActivity.this);
            }
        });
    }

    @Override
    public void doAfter() {
        GsonRequest gsonObjectRequest = new GsonRequest(PullRequest.getUrl(mRepository.getOwner().getLogin(), mRepository.getName()), PullRequest[].class, null, new Response.Listener<PullRequest[]>() {
            @Override
            public void onResponse(PullRequest[] response) {
                pullRequestAdapter.addList(Arrays.asList(response));
                llCountPullRequests.setVisibility(View.VISIBLE);

                int openedCount = 0;
                int closedCount = 0;

                for (Map.Entry<Integer, Integer> entry : ((PullRequestAdapter) rvGithub.getAdapter()).getOpenedAndClosedPullRequestsCount().entrySet()) {
                    openedCount = entry.getKey();
                    closedCount = entry.getValue();
                }

                tvCountPullRequests.setText(getString(R.string.count_pull_requests, rvGithub.getAdapter().getItemCount(), openedCount, closedCount));

                srlGithub.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(gsonObjectRequest);
    }
}
