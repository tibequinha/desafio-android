package br.com.motta.douglas.apigithubjava.activity;

import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.motta.douglas.apigithubjava.R;

@EActivity(R.layout.activity_base)
public class BaseActivity extends AppCompatActivity {
    @ViewById
    protected Toolbar mainToolbar;

    protected void setUpToolbar(String title) {
        if (mainToolbar != null) {
            mainToolbar.setTitle(title);
            setSupportActionBar(mainToolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
