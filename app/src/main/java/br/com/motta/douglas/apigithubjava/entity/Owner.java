package br.com.motta.douglas.apigithubjava.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by douglas on 22/03/17.
 */

public class Owner implements Parcelable {
    private String login;
    private String avatarUrl;
    private String type;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.login);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.type);
    }

    public Owner() {
    }

    protected Owner(Parcel in) {
        this.login = in.readString();
        this.avatarUrl = in.readString();
        this.type = in.readString();
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel source) {
            return new Owner(source);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };
}
