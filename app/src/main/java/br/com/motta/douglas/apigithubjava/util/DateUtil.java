package br.com.motta.douglas.apigithubjava.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by douglas on 24/03/17.
 */

public class DateUtil {
    public static final String PATTERN_DB = "yyyy-MM-dd";

    public static String formatStringDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_DB, Locale.ENGLISH);
        try {
            SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            Date date = parse.parse(strDate);

            return formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
