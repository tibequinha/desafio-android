package br.com.motta.douglas.apigithubjava.interfaces;

/**
 * Created by douglas on 23/03/17.
 */

public interface Transaction {
    void doAfter();
}
