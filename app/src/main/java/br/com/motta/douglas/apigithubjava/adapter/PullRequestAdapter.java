package br.com.motta.douglas.apigithubjava.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.motta.douglas.apigithubjava.R;
import br.com.motta.douglas.apigithubjava.entity.PullRequest;
import br.com.motta.douglas.apigithubjava.entity.VolleySingleton;
import br.com.motta.douglas.apigithubjava.util.DateUtil;

/**
 * Created by douglas on 23/03/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter {
    private List<PullRequest> mPullRequests;
    private Context mContext;
    private PullRequestAdapter.OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }


    public void setOnItemClickListener(PullRequestAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests) {
        this.mPullRequests = pullRequests;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.mContext).inflate(R.layout.item_pull_request, parent, false);

        return new PullRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PullRequestViewHolder repositoryHolder = (PullRequestAdapter.PullRequestViewHolder) holder;

        PullRequest pullRequest = mPullRequests.get(position);

        if (pullRequest != null) {
            repositoryHolder.tvTitle.setText(pullRequest.getTitle());
            repositoryHolder.tvCreatedAt.setText(DateUtil.formatStringDate(String.valueOf(pullRequest.getCreatedAt())));
            repositoryHolder.tvBody.setText(pullRequest.getBody() != null && pullRequest.getBody().length() > 60 ? pullRequest.getBody().substring(0, 60) + "..." : pullRequest.getBody());
            repositoryHolder.tvLogin.setText(pullRequest.getUser().getLogin());
            repositoryHolder.ivAvatar.setImageUrl(pullRequest.getUser().getAvatarUrl(), VolleySingleton.getInstance(mContext).getImageLoader());
        }
    }

    @Override
    public int getItemCount() {
        if (this.mPullRequests != null) {
            return this.mPullRequests.size();
        }

        return 0;
    }

    public Map<Integer, Integer> getOpenedAndClosedPullRequestsCount() {
        int opened = 0;
        int closed = 0;

        if (this.mPullRequests != null && this.mPullRequests.size() > 0) {
            for (PullRequest pullRequest : this.mPullRequests) {
                if (TextUtils.equals(pullRequest.getState(), PullRequest.OPENED)) {
                    opened++;
                } else if (TextUtils.equals(pullRequest.getState(), PullRequest.CLOSED)) {
                    closed++;
                }
            }
        }

        Map<Integer, Integer> openedAndClosedCount = new HashMap<>();
        openedAndClosedCount.put(opened, closed);

        return openedAndClosedCount;
    }


    public void addList(List<PullRequest> pullRequests) {
        mPullRequests.addAll(pullRequests);
        notifyDataSetChanged();
    }

    class PullRequestViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvBody;
        TextView tvCreatedAt;
        TextView tvLogin;
        NetworkImageView ivAvatar;

        public PullRequestViewHolder(final View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvBody = (TextView) itemView.findViewById(R.id.tvBody);
            tvCreatedAt = (TextView) itemView.findViewById(R.id.tvCreatedAt);
            tvLogin = (TextView) itemView.findViewById(R.id.tvLogin);
            ivAvatar = (NetworkImageView) itemView.findViewById(R.id.ivAvatar);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
