package br.com.motta.douglas.apigithubjava.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by douglas on 21/03/17.
 */

public class Repository implements Parcelable {
    private static final String URI_REPOSITORIES = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";

    private String name;
    private String description;
    private int stargazersCount;
    private int forksCount;
    private Owner owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public static String getUrl(int page) {
        return URI_REPOSITORIES + page;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.stargazersCount);
        dest.writeInt(this.forksCount);
        dest.writeParcelable(this.owner, flags);
    }

    protected Repository(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.stargazersCount = in.readInt();
        this.forksCount = in.readInt();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
