package br.com.motta.douglas.apigithubjava;

import android.app.Application;
import android.util.Log;

/**
 * Created by douglas on 25/03/17.
 */

public class ApiGithubApplication extends Application {
    private static final String TAG = "ApiGithubApplication";
    private static ApiGithubApplication instance = null;

    public static ApiGithubApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "ApiGithubApplication.onCreate()");

        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        Log.d(TAG, "ApiGithubApplication.onTerminate()");
    }
}
